"""
The base module of io.

This module contains the base classes for input and output.

All classes in this module should be called directly via expyriment.io.*.

"""

__author__ = 'Florian Krause <florian@expyriment.org>, \
Oliver Lindemann <oliver@expyriment.org>'
__version__ = '0.7.0'
__revision__ = '55a4e7e'
__date__ = 'Wed Mar 26 14:33:37 2014 +0100'

import expyriment

class Input(expyriment._Expyriment_object):
    """A class implementing a general input."""

    def __init__(self):
        """Create an input."""
        expyriment._Expyriment_object.__init__(self)


class Output(expyriment._Expyriment_object):
    """A class implementing a general output."""

    def __init__(self):
        """Create an output."""
        expyriment._Expyriment_object.__init__(self)
